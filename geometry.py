import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

def normalize_vector(v):
  return v / (v**2).sum()**.5

def arrow(ax, start, end, linewidth = 1., color = "k", label = None, offset = (3, 0), style = "->", shrinkA = 0, shrinkB = 0, alpha = 1., loc = "mid"):
  x0 = start[0]
  x1 = end[0]
  y0 = start[1]
  y1 = end[1]
  
  ax.annotate("",end, start, arrowprops=dict(arrowstyle=style, color = color, shrinkA=shrinkA, shrinkB=shrinkB, alpha = alpha)                            
                    )
  if label != None:
    if loc == "mid": xy = ((x0 + x1)/2., (y0 + y1)/2.)
    if loc == "start": xy = (x0 , y0)
    if loc == "end": xy = (x1 , y1)
    ax.annotate(
       label, xy=xy, xycoords='data',
        xytext=offset, textcoords='offset points', color = color, alpha = alpha)
        
def force_repartie(ax, x, f, xl = None, color = "red", linewidth = 1., limit = .1, label = None, offset = (0,5)):
  y = f(x)
  ax.fill_between(x, 0., y, color = color, alpha = .5, edgecolor = "none") 
  y = np.where(abs(y) <= limit, np.nan, y)
  ax.plot(x,y, color = color, linewidth = linewidth)
  if xl == None: xl = x.mean()
  if label != None:
    ax.annotate(
         label, xy=(xl, f(xl)), xycoords='data',
          xytext=offset, textcoords='offset points', color = color) 
          
          
          
          
def point(ax, x, y=0., label = None, color = "k", offset = (5,5), alpha = 1., 
          r = 0.05):
  if color != None:
    circle = plt.Circle((x, y), r, color= color, alpha = alpha)
    ax.add_artist(circle)          
  if label != None:
    ax.annotate(
         label, xy=(x, y), xycoords='data',
          xytext=offset, textcoords='offset points', color = color, alpha = alpha)         
          
          
def angle(ax, center, radius = 1., a0 = 0., a1 = 90., offset = 0.5, color = "k",
          linewidth = 1., label = None, head_width = 0.2, head_length = 0.4, 
          style = "->"):
  xc = center[0]
  yc = center[1]
  na = 100
  a = np.radians(np.linspace(a0+offset, a1-offset, na))
  
  
  x = xc + radius * np.cos(a) 
  y = yc + radius * np.sin(a) 
  ax.plot(x, y, color = color, linewidth = linewidth)
  ax.arrow(x[-2], y[-2], x[-1]-x[-2], y[-1]-y[-2], length_includes_head=True,
  fc=color, ec=color,head_width = head_width, head_length= head_length, )
  if label != None:
    xl = x[int(na/2)]
    yl = y[int(na/2)]
    ax.annotate( label, xy=(xl, yl), xycoords='data', xytext=(5, 5), textcoords='offset points', color = color) 
  
  
def angle3d(ax, center, p1, p2, perspective, radius = 1., offset = 0., color = "k", linewidth = 1., label = None, label_offset = (5,5)):
  xc = center[0]
  yc = center[1]
  center = np.array(center)
  p1, p2 = np.array(p1), np.array(p2)
  u1 = normalize_vector(p1 - center)
  u2 = normalize_vector(p2 - center)
  axis = np.cross(u1, u2)
  angle = np.degrees(np.arcsin((axis**2).sum()**.5))
  if np.dot(u1, u2) < 0. : 
    angle += np.pi/2.
  axis = normalize_vector(axis)
  e1 = u1
  e2 = normalize_vector(u2 - np.dot(u1, u2) * u1)
  e3 = axis
  na = 100
  a = np.radians(np.linspace(offset, angle-offset, na))
  points = np.zeros((3,na))
  for i in xrange(na):
    points[:,i] = center + radius * (np.cos(a[i]) * e1 + np.sin(a[i]) * e2)
  points2d = perspective(points)
  x = points2d[0]
  y = points2d[1]
  ax.plot(x, y, color = color, linewidth = linewidth)
  hdir = points[:,-1] - points[:,-2]
  #hdir = np.array([x[-1] - x[-2], y[-1] - y[-2]])
  hdir /= (hdir**2).sum()**.5
  hnor = u2
  
  htip = points[:,-1]
  hw = .3
  hl = .6
  vert = [htip, htip - hl * hdir + .5 * hw * hnor , htip - hl * hdir - .5 * hw * hnor ] 
  vert2d = [perspective(v) for v in vert]
  head = patches.Polygon(vert2d, facecolor = color, edgecolor = color)
  ax.add_artist(head)
  if label != None:
    xl = x[na/2]
    yl = y[na/2]
    ax.annotate( label, xy=(xl, yl), xycoords='data', xytext=label_offset, textcoords='offset points', color = color)   
    
    
 
   
      
