from mechanics.links import encastrement, ponctuelle
from matplotlib import pyplot as plt

fig = plt.figure(0)
plt.clf()
ax = fig.add_subplot(1,1,1)
ax.set_aspect("equal")
plt.plot([0., 10.],  [0., 0.], "k-", linewidth = 2.)
encastrement(ax, (0,0))
ponctuelle(ax, (5,0))
plt.show()
