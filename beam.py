import numpy as np
from scipy.interpolate import interp1d
from scipy.misc import derivative
import copy


class Poutre(object):
  def __init__(self, L, section = None, generatrice = None, Ux =  None, Uy = None, Uz = None, Rx = None, links = []):
    self.L = L
    self.section     = section
    self.generatrice = generatrice
    if Ux == None: Ux = lambda x: 0.*x
    if Uy == None: Uy = lambda x: 0.*x
    if Uz == None: Uz = lambda x: 0.*x
    if Rx == None: Rx = lambda x: 0.*x
    self.Ux = Ux
    self.Uy = Uy
    self.Uz = Uz
    self.Rx = Rx
    self.links = links
  
  def add_linkage(self, link, x):
    self.links.append(["link", x])
  
  def get_shape(self, nx = 100, ns = 10, deformed = True):
    x = np.linspace(0., self.L, nx) 
    if deformed:
      Ux = self.Ux
      Uy = self.Uy
      Uz = self.Uz
      Rx = self.Rx
    else:
      Ux = lambda x:0.*x
      Uy = lambda x:0.*x
      Uz = lambda x:0.*x
      Rx = lambda x:0.*x
      
    ux = Ux(x)
    uy = Uy(x)
    uz = Uz(x)
    rx = Rx(x)  
    dx = x[1]
    ry = np.arctan(np.array([derivative(Uz, t) for t in x]))
    rz = np.arctan(np.array([derivative(Uy, t) for t in x]))
    #rz = np.arctan(np.gradient(uy, dx))
    #ry = np.arctan(np.gradient(uz, dx))
    
    
    if self.section != None:
      sections = []
      section = self.section
      xs = np.linspace(0., self.L, ns)
      uxs = Ux(xs)
      uys = Uy(xs)
      uzs = Uz(xs)
      rxs = Rx(xs)
      
      Ry = interp1d(x,ry)
      Rz = interp1d(x,rz)
      rys = Ry(xs)
      rzs = Rz(xs)
      one = np.ones_like(rxs)
      zero = np.zeros_like(rxs)
      Mx = np.array([[one, zero, zero] , [zero, np.cos(rxs), -np.sin(rxs)] , [zero,np.sin(rxs), np.cos(rxs)]])
      My = np.array([[np.cos(rys), zero, np.sin(rys)] , [zero, one, zero] , [-np.sin(rys),zero, np.cos(rys)]])
      Mz = np.array([[np.cos(rzs), -np.sin(rzs), zero] , [np.sin(rzs), np.cos(rzs), zero] , [zero, zero , one]])
      for i in xrange(ns):
        sxi, syi, szi = [], [], []
        M = np.dot(np.dot(Mx[:,:,i], My[:,:,i]), Mz[:,:,i])
        si = section(xs[i])
        sy = si[0]
        sz = si[1]
        for j in xrange(len(sy)):
          v = np.array([0., sy[j], sz[j]])
          v = np.dot(M, v)
          sxi.append(v[0] + uxs[i]+ xs[i])
          syi.append(v[1] + uys[i])
          szi.append(v[2] + uzs[i])
        s = np.array([sxi, syi, szi])  
        sections.append(s)
    else:
      sections = None
    
    if self.generatrice != None:
      generatrice = self.generatrice
      ng = len(generatrice(0)[0])
      generatrices = np.zeros([ng, 3, nx])
      xg = np.linspace(0., self.L, nx)
      uxg = ux
      uyg = uy
      uzg = uz
      rxg = rx
      ryg = ry
      rzg = rz
      one = np.ones_like(rxg)
      zero = np.zeros_like(rxg)
      Mx = np.array([[one, zero, zero] , [zero, np.cos(rxg), -np.sin(rxg)] , [zero,np.sin(rxg), np.cos(rxg)]])
      My = np.array([[np.cos(ryg), zero, np.sin(ryg)] , [zero, one, zero] , [-np.sin(ryg),zero, np.cos(ryg)]])
      Mz = np.array([[np.cos(rzg), -np.sin(rzg), zero] , [np.sin(rzg), np.cos(rzg), zero] , [zero, zero , one]])
      for i in xrange(len(xg)):
        gxi, gyi, gzi = [], [], []
        M = np.dot(np.dot(Mx[:,:,i], My[:,:,i]), Mz[:,:,i])
        gen = generatrice(xg[i])
        gy = gen[0]
        gz = gen[1]
        for j in xrange(ng):
          v = np.array([0., gy[j], gz[j]])
          v = np.dot(M, v)
          gxi.append(v[0] + uxg[i]+ xg[i])
          gyi.append(v[1] + uyg[i])
          gzi.append(v[2] + uzg[i])
        for j in xrange(ng):
          generatrices[j,0,i] = gxi[j]
          generatrices[j,1,i] = gyi[j]
          generatrices[j,2,i] = gzi[j]
       
    else:
      generatrices = None             
    return x, ux, uy, uz, rx, ry, rz, sections,generatrices
    
